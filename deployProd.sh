#!/bin/bash
CONTAINER_NAME="tp5gbjlflask"
CONTAINER_EXISTS=$(docker ps -a | grep $CONTAINER_NAME | wc -l)
if (($CONTAINER_EXISTS > 0)); then
	docker stop $CONTAINER_NAME
	docker rm -f $CONTAINER_NAME
fi

IMAGE_NAME="tp5gbjlflask_image"
IMAGE_EXISTS=$(docker images | grep $IMAGE_NAME | wc -l)
if (($IMAGE_EXISTS > 0)); then
	docker image rm -f $IMAGE_NAME
fi

VOLUME_NAME="tp5gbjlflask_vol"
VOLUME_EXISTS=$(docker volume ls | grep $VOLUME_NAME | wc -l)
if (($VOLUME_EXISTS > 0)); then
	docker volume rm -f $VOLUME_NAME
fi

docker volume create --name $VOLUME_NAME --opt device=$PWD --opt o=bind --opt type=none
docker build -t $IMAGE_NAME -f ./project/docker/DockerfileAPI .
docker run -d -p 5555:5555 -e INFRA_TP5_DB_TYPE='MYSQL' -e INFRA_TP5_DB_HOST='db-tp5-infra.ddnsgeek.com' -e INFRA_TP5_DB_PORT='7777' -e INFRA_TP5_DB_USER='produser' -e INFRA_TP5_DB_PASSWORD='3ac4d0b0e24871436f45275890a458c6' --mount source=$VOLUME_NAME,destination=/mnt/app/ --name $CONTAINER_NAME $IMAGE_NAME
