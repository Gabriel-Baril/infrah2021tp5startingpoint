echo "Running deployment tests"
python3 -m unittest discover -s project -v -p dtest_*.py
if [ $? -ne 0 ]; then
        echo "Deployment tests failed"
        exit 1
fi
