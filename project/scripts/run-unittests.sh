echo "Running unit tests with coverage stats"
coverage run --omit */test_*,*/dtest_*,*__init__.py,/usr/lib/python3/dist-packages/* -m unittest discover -s project -p test*.py -v
if [ $? -ne 0 ]; then
        echo "Unit tests failed"
        exit 1
fi
REPORT_DIR="reporting/coverage"
coverage html -d $REPORT_DIR/html
touch $REPORT_DIR/report.txt
coverage report -m > $REPORT_DIR/report.txt

mv .coverage $REPORT_DIR/.coverage
